#include "list.h"
//创建节点
Node* create_node(void* ptr)
{
	Node* node =malloc(sizeof(Node));
	node->ptr=ptr;
	node->next=NULL;
	return node;

}
//创建链表
List* create_list(void)
{
	List* list=malloc(sizeof(List));
	list->head=NULL;
	list->size=0;
	return list;
}
//销毁
void destory_list(List* list)
{
	clear_list(list);
	free(list);
}
//清空链表
void clear_list(List* list)
{
	while(NULL!=list->head)
	{	
		Node* node=list->head;
		del_head(list);
		free(node);
	}

}
//头添加
void  add_head(List* list,void* ptr)
{
	Node* new =create_node(ptr);
	
	new->next=list->head;
	list->head=new;
	list->size++;
}
//尾添加
void add_tail(List* list,void* ptr)
{

	Node* node=create_node(ptr);
	if(list->size==0)
	{
		list->head=node;
	}
	else
	{
		Node* tail=list->head;
		while(NULL!=tail->next)
		{
			tail=tail->next;
		}
		tail->next=node;
	}
	list->size++;
}
//指定位置添加
bool add_index(List* list,void* ptr,int index)
{

	if(index<0||index>=list->size)return false;
	Node* new=create_node(ptr);
	Node* prev=list->head;
	if(index==0)
	{
		add_head(list,ptr);
	}
	else{
		for(int i=0;i<index-1;i++)
		{
			prev=prev->next;
		}
		new->next=prev->next;
		prev->next=new;
	}
	list->size++;
	return true;
}
//头删除
bool del_head(List* list)
{
	if(list->size<=0)return false;
	
	Node* node=list->head;
	list->head=node->next;
	free(node);
	list->size--;
	if(list->size==1)
	{
		free(list->head);
		list->size--;
	}

	
}
//尾删除
bool del_tail(List* list)
{

	if(list->size<=0)return false;
	if(1==list->size)
	{
		free(list->head);
		list->head=NULL;
		list->size--;
		return true;
	}
	Node* tail=list->head;
	while(NULL!=tail->next)
	{
		tail=tail->next;
	}
	Node* prev=list->head;
	while(prev->next!=tail)
	{
		prev=prev->next;
	}
	prev->next=NULL;
	free(tail);
	list->size--;
	return true;
}
//指定位置删除
bool del_index(List* list,int index)
{
	if(index<0||index>=list->size)return false;

	if(index==0)
	{
		return del_head(list);
	}
	if(index==list->size-1)return del_tail(list);

	Node* prev=list->head;
	for(int i=0;i<index-1;i++)
	{
		prev=prev->next;
	}
	Node* node=prev->next;
	prev->next=node->next;
	free(node);
	list->size--;
	return true;


}
//指定值删除
bool del_val(List* list,void* ptr,compar cmp)
{
	if(list->size<=0)return false;
	int times=0;
	Node* prev=NULL;
	for(prev =list->head;prev->next->next!=NULL;prev=prev->next)
	{
		
		if(0==cmp(list->head->ptr,ptr))
		{
			del_head(list);
			times++;
		}
		if(0==cmp(prev->next->ptr,ptr))
		{
			Node* node=prev->next;
			prev->next=node->next;
			free(node);
			list->size--;
			times++;
		}
	}	

	//double *temp=prev->ptr;
	//printf("last prev=%lf\n",*temp);
	//double *temp_ptr=ptr;
	//printf("temp ptr=%lf\n",*temp_ptr);
	//printf("flag=%d\n",cmp(prev->ptr,ptr));
	
	prev=prev->next;
	if(0==cmp(ptr,prev->ptr))
	{
		del_tail(list);
		times++;
		//printf("havein\n");
	}

	if(times>0)
		return true;
	else return false;
	
}
//指定位置访问|返回值
void* access_index(List* list,int index)
{

	Node* node=list->head;
	for(int i=0;i<index;i++)
	{
		node=node->next;
	}
	return &node->ptr;
}
//指定值访问|返回位置
int access_val(List* list,void* ptr,compar cmp)
{
	int index=0;
	for(Node* node =list->head;NULL!=node;node=node->next)
	{
		if(0==cmp(node->ptr,ptr))
			{
				return index;
			}
		index++;
	}
	return -1;
}


//排序
void sort_list(List* list,compar cmp)
{
	for(Node* i=list->head;NULL!=i->next;i=i->next)
	{
		for(Node* j=i->next;NULL!=j;j=j->next)
		{
			if(1==cmp(i->ptr,j->ptr))
			{
				void* temp=i->ptr;
				i->ptr=j->ptr;
				j->ptr=temp;
			}
		}
	}
}
//遍历
void show_list(List* list,void(*show)(const void* ptr))
{
	
	for(Node* node=list->head;NULL!=node;node=node->next)
	{
		show(node->ptr);
	}
	printf("\n");
}

